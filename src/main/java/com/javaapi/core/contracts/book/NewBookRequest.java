package com.javaapi.core.contracts.book;

import java.sql.Date;

public class NewBookRequest 
{
    public String title;

    public int authorId;

    public int pages;

    public Date published;


}
