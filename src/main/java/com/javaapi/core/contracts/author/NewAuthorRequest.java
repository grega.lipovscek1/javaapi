package com.javaapi.core.contracts.author;

public class NewAuthorRequest 
{
    public String name;

    public String lastname;

    public int age;

    public boolean alive;

}
