package com.javaapi.core.reposatory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javaapi.core.entity.Author;


public interface AuthorReposatory extends JpaRepository<Author, Long>
{
    
}
