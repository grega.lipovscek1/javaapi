package com.javaapi.core.reposatory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javaapi.core.entity.Book;


public interface BookReposatory extends JpaRepository<Book, Long>
{
    
}
