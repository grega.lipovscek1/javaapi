package com.javaapi.core.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javaapi.core.services.StatusService;

@RestController
public class StatusController 
{   
    private final StatusService statusService;

    public StatusController( StatusService statusService )
    {
        this.statusService = statusService;
    }

    @GetMapping("/check")
    public String statusController()
    {
        return statusService.checkSystem();
    }    
}
