package com.javaapi.core.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javaapi.core.contracts.book.NewBookRequest;
import com.javaapi.core.entity.Book;
import com.javaapi.core.services.BookService;

@RestController
public class BookController 
{   
    private final BookService bookService;

    public BookController( BookService bookService)
    {
        this.bookService = bookService;
    }
    
    @PostMapping("/book")
    public void newBookRequest( @RequestBody NewBookRequest newBookRequest )
    {   
        this.bookService.addNewBook( newBookRequest );
    }

    @GetMapping("/books")
    public List<Book> newBookRequest( )
    {   
        return this.bookService.getList(  );
    }
    @GetMapping("/book/{bookid}")
    public Optional<Book> getAuthorById( @PathVariable("bookid") Long id)
    {
        return this.bookService.getBookById( id );
    }
    @DeleteMapping("/book/{bookid}")
    public void deleteBookById( @PathVariable("bookid") Long id)
    {
        this.bookService.deleteBookById( id );
    }
}
