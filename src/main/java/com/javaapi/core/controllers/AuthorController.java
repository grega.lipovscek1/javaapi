package com.javaapi.core.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.javaapi.core.contracts.author.NewAuthorRequest;
import com.javaapi.core.entity.Author;
import com.javaapi.core.services.AuthorService;

@RestController
public class AuthorController
{   
    private final AuthorService authorService;

    public AuthorController( AuthorService authorService )
    {
        this.authorService = authorService;
    }
    
    @GetMapping("/authors")
    public List<Author> getAuthors()
    {
        return this.authorService.getList();
    }
    @PostMapping("/author")
    public ResponseEntity<HttpStatus> newAuthorRequest( @RequestBody  NewAuthorRequest newAuthorRequest )
    {
        this.authorService.addAuthor( newAuthorRequest );
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping("/author/{authorId}")
    public Optional<Author> getAuthorById( @PathVariable("authorId") Long id)
    {   
        return this.authorService.getAuthorById( id );
    }
    @DeleteMapping("/author/{authorId}")
    public void deleteAuthorById( @PathVariable("authorId") Long id)
    {
        this.authorService.deleteAuthorById( id );
    }
}
