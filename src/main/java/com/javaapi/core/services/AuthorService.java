package com.javaapi.core.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.javaapi.core.contracts.author.NewAuthorRequest;
import com.javaapi.core.entity.Author;
import com.javaapi.core.reposatory.AuthorReposatory;



@Service
public class AuthorService 
{
    private final AuthorReposatory authorReposatory;

    public AuthorService( AuthorReposatory authorReposatory )
    {
        this.authorReposatory = authorReposatory;
    }

    public void addAuthor( NewAuthorRequest newAuthorRequest) 
    {   
       Author author = new Author(newAuthorRequest.name, newAuthorRequest.lastname, newAuthorRequest.age, newAuthorRequest.alive);
       this.authorReposatory.save(author);
    }

    public List<Author> getList() 
    {
        return this.authorReposatory.findAll();
    }
    public Optional<Author> getAuthorById( Long authorId ) 
    {
        return this.authorReposatory.findById( authorId );
    } 
    public void deleteAuthorById( Long authorId ) 
    {
        this.authorReposatory.deleteById( authorId );
    }    
    
    
}
