package com.javaapi.core.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.javaapi.core.contracts.book.NewBookRequest;
import com.javaapi.core.entity.Book;
import com.javaapi.core.reposatory.BookReposatory;

@Service
public class BookService 
{   
    private final BookReposatory bookReposatory;

    public BookService( BookReposatory bookReposatory)
    {
        this.bookReposatory = bookReposatory;
    }

    public void addNewBook( NewBookRequest newBookRequest)
    {
        Book book = new Book(newBookRequest.title, newBookRequest.authorId, newBookRequest.pages, newBookRequest.published );
        this.bookReposatory.save( book );
    }
    public List<Book> getList() 
    {
        return this.bookReposatory.findAll();
    }
    public Optional<Book> getBookById( Long bookId ) 
    {
        return this.bookReposatory.findById( bookId );
    }
    public void deleteBookById( Long bookId ) 
    {
        this.bookReposatory.deleteById( bookId );
    }   
}
