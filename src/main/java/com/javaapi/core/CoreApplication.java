package com.javaapi.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoreApplication {

	public static void main(String[] args) {
		try
		{
			SpringApplication.run(CoreApplication.class, args);
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

}
