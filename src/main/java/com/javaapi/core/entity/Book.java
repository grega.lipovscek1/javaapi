package com.javaapi.core.entity;

import java.sql.Date;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;

@Entity
@Table
public class Book 
{
    @Id
    @SequenceGenerator(
        name="book_sequence",
        sequenceName="book_sequence",
        allocationSize=1
    )
    @GeneratedValue(
        strategy= GenerationType.SEQUENCE,
        generator= "book_sequence"
    )

    private Long Id; 
    
    private String Title;

    private int Publisher;

    private Date PublishDate;

    private int Pages;

    public Book( String title, int authorId, int pages, Date publishDate)
    {
        this.Title = title;
        this.Publisher = authorId;
        this.Pages = pages;
        this.PublishDate = publishDate;

    }
    public Book( )
    {
    }
    public Long getId()
    {
        return this.Id;
    }
    public String getTitle()
    {
        return this.Title;
    }
    public void setTitle( String title)
    {
        this.Title = title;
    }
    public int getAuthorId()
    {
        return this.Publisher;
    }
    public void setAuthorId( int auhtorId )
    {
        this.Publisher = auhtorId;
    }
    public int getPages()
    {
        return this.Pages;
    }
    public void setPages( int pages )
    {
        this.Pages = pages;
    }
    public Date getDate()
    {
        return this.PublishDate;
    }
    public void setDate( Date date )
    {
        this.PublishDate = date;
    }
    @Override
	public String toString() {
		return "Book [id=" + Id + ", title=" + this.Title + "]";
	}

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Book))
            return false;
        Book book = (Book)o;
        return Pages == book.Pages && Objects.equals(Title, book.Title) && Objects.equals(this.Publisher, book.Publisher)
        && Objects.equals(this.PublishDate, book.PublishDate);
    }
    @Override
    public int hashCode()
    {
        return Objects.hash(Title,Publisher, Pages, PublishDate);
    }
}
