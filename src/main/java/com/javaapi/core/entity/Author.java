package com.javaapi.core.entity;

import java.util.Objects;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;

@Entity
@Table 
public class Author 
{   
    
    @SequenceGenerator(
        name="author_sequence",
        sequenceName="author_sequence",
        allocationSize=1
    )
    @GeneratedValue(
        strategy= GenerationType.SEQUENCE,
        generator= "author_sequence"
    )

    @Id
    private Long Id; 
    
    private String Name;

    private String Lastname;

    private int Age;

    private boolean Alive;

    public Author(String name, String lastname, int age, boolean alive)
    {   
        this.Name = name;
        this.Lastname = lastname;
        this.Age = age;
        this.Alive = alive;
    }
    public Author()
    {   
    }

    public Long getId()
    {
        return this.Id;
    }
    public void setId( Long id)
    {
        this.Id = id;
    }
    public String getName()
    {
        return this.Name;
    }
    public void setName( String name)
    {
        this.Name = name;
    }
    public String getLastName()
    {
        return this.Lastname;
    }
    public void setLastName( String lastname)
    {
        this.Lastname = lastname;
    }
    public int getAge()
    {
        return this.Age;
    }
    public void setAge( int age)
    {
        this.Age = age;
    }
    public boolean getAlive()
    {
        return this.Alive;
    }
    public void setAge( boolean alive)
    {
        this.Alive = alive;
    }

    @Override
	public String toString() {
		return "Author [id=" + Id + 
        ", name=" + this.Name + 
        ", lastname=" + this.Lastname + 
        ", age=" + this.Age + 
        ", alive=" + this.Alive + 
        "]";
	}
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Book))
            return false;
        Author author = (Author)o;
        return Age == author.Age && Alive ==  author.Alive && Objects.equals(Name, author.Name);
       
    }
    @Override
    public int hashCode()
    {
        return Objects.hash(Name,Lastname, Age, Alive);
    }

}
